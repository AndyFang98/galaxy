[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/Magmoz/galaxy/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

# Table of Contents
- [Getting started](#getting-started)
- [Installation](#installation)
- [Reporting a Bug](#reporting-a-bug)

## Getting Started

## Installation

You can either clone Galaxy to your project, or use the CDN linking to the latest version:
`<link rel="stylesheet" href="//cdn.rawgit.com/Magmoz/galaxy/master/galaxy.min.css">`
This will always link to the latest version of Galaxy.


## Reporting a Bug


Galaxy was created and licensed under the [MIT](//tldrlegal.com/license/mit-license) by [Andy Fang](//twitter.com/andyfang98).



